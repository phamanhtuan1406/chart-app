import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DefaultLayoutComponent} from './layouts/default-layout/default-layout.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('src/app/layouts/default-layout/default-layout.module').then(m => m.DefaultLayoutModule)
      },
      {
        path: 'them-mau-bao-cao',
        loadChildren: () => import('src/app/pages/them-mau-bao-cao/them-mau-bao-cao.module').then(m => m.ThemMauBaoCaoModule),
      },
      {
        path: 'tong-hop',
        loadChildren: () => import('src/app/pages/tong-hop-bao-cao/tong-hop-bao-cao.module').then(m => m.TongHopBaoCaoModule),
      },
    ]
  },
  {
    path: '**',
    redirectTo: 'dashboard'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
