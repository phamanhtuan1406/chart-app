import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TongHopBaoCaoComponent } from './tong-hop-bao-cao.component';

describe('TongHopBaoCaoComponent', () => {
  let component: TongHopBaoCaoComponent;
  let fixture: ComponentFixture<TongHopBaoCaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TongHopBaoCaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TongHopBaoCaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
