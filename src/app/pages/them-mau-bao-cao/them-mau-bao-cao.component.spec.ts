import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThemMauBaoCaoComponent } from './them-mau-bao-cao.component';

describe('ThemMauBaoCaoComponent', () => {
  let component: ThemMauBaoCaoComponent;
  let fixture: ComponentFixture<ThemMauBaoCaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThemMauBaoCaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemMauBaoCaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
