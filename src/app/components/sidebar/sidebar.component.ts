import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Output() showNavBar: EventEmitter<any> = new EventEmitter<any>();

  avatar: string;
  accountName: string;
  numberInfo: number;

  constructor() {
    this.avatar = 'https://thuthuatnhanh.com/wp-content/uploads/2019/05/gai-xinh-toc-ngan-facebook.jpg'
    this.accountName = 'Admin'
    this.numberInfo = 2
  }

  ngOnInit(): void {
  }

}
