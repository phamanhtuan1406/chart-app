import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-chart-render',
  templateUrl: './chart-render.component.html',
  styleUrls: ['./chart-render.component.css']
})
export class ChartRenderComponent implements OnInit {
  value1: number

  constructor() {
    this.value1 = 0
  }

  ngOnInit(): void {
  }

}
