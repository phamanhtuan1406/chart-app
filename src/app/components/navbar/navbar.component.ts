import {Component, OnInit} from '@angular/core';

import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  listMenu: MenuItem[];
  PATH_NAME: string;

  constructor() {
    this.listMenu = [
      {
        label: 'DashBoard',
        url: '/dashboard',
        icon: 'pi pi-th-large',
        expanded: false
      },
      {
        label: 'Báo cáo',
        icon: 'pi pi-chart-bar',
        expanded: false,
        items: [
          {
            label: 'Thêm mẫu báo cáo',
            url: '/them-mau-bao-cao'
          }
          ,
          {
            label: 'Tổng hợp',
            url: '/tong-hop'
          }
        ]
      }
    ];
    this.PATH_NAME = '';
  }

  ngOnInit(): void {
    this.setCurrentPath()

    for (let i = 0; i < this.listMenu.length; i++) {
      if (this.listMenu[i].url === this.PATH_NAME) {
        this.setMenuActive(i)
      }

      if (!!this.listMenu[i].items?.length) {
        const PATH_NAME = window.location.pathname
        this.listMenu[i].items?.forEach((item, index) => {

          if (item.url === PATH_NAME) {
            this.setMenuActive(i)
          }
        })
      }
    }
  }

  setMenuActive(index: number) {
    this.setCurrentPath()
    let arr = [...this.listMenu]

    for (let i = 0; i < arr.length; i++) {
      if (i === index) {
        arr[i].expanded = true
      } else {
        // arr[i].expanded = false
      }
    }

    this.listMenu = [...arr]
  }

  setCurrentPath() {
    setTimeout(() => {
      this.PATH_NAME = window.location.pathname
    }, 100)
  }

}
