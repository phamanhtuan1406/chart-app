import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ButtonModule} from 'primeng/button';
import {AvatarModule} from 'primeng/avatar';
import {ImageModule} from 'primeng/image';
import {BadgeModule} from 'primeng/badge';
import {MenuModule} from 'primeng/menu';
import {PanelMenuModule} from 'primeng/panelmenu';
import {InputTextModule} from "primeng/inputtext";

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [
    ButtonModule,
    AvatarModule,
    ImageModule,
    BadgeModule,
    MenuModule,
    PanelMenuModule,
    InputTextModule
  ]
})
export class SharedModule {
}
