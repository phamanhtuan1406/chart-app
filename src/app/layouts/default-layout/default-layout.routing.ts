import {Routes} from '@angular/router';

import {DashboardComponent} from '../../pages/dashboard/dashboard.component';
import {ThemMauBaoCaoComponent} from '../../pages/them-mau-bao-cao/them-mau-bao-cao.component';
import {TongHopBaoCaoComponent} from '../../pages/tong-hop-bao-cao/tong-hop-bao-cao.component';

export const AdminLayoutRoutes: Routes = [
  {path: 'dashboard', component: DashboardComponent},
  {path: 'them-mau-bao-cao', component: ThemMauBaoCaoComponent},
  {path: 'tong-hop', component: TongHopBaoCaoComponent},
];
