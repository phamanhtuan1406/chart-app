import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {SharedModule} from "./shared/shared.module";

import {AppComponent} from './app.component';
import {DefaultLayoutComponent} from './layouts/default-layout/default-layout.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {FooterComponent} from './components/footer/footer.component';
import { MainContentComponent } from './components/main-content/main-content.component';
import { ThemMauBaoCaoComponent } from './pages/them-mau-bao-cao/them-mau-bao-cao.component';
import { TongHopBaoCaoComponent } from './pages/tong-hop-bao-cao/tong-hop-bao-cao.component';
import { ChartRenderComponent } from './components/chart-render/chart-render.component';

@NgModule({
  declarations: [
    AppComponent,
    DefaultLayoutComponent,
    DashboardComponent,
    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    MainContentComponent,
    ThemMauBaoCaoComponent,
    TongHopBaoCaoComponent,
    ChartRenderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
